﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TargetBorder
{
    class Program
    {
        public static int[,] GetBorderTargets(int[,] visitedMap)
        {
            int[,] border = { { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 } };
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (visitedMap[i,j] == 1)
                    {
                        continue;
                    }
                    if (i > 0 && visitedMap[i - 1, j] == 1)
                    {
                        border[i, j] = 1;
                        continue;
                    }
                    if (i < 2 && visitedMap[i + 1, j] == 1)
                    {
                        border[i, j] = 1;
                        continue;
                    }
                    if (j > 0 && visitedMap[i, j - 1] == 1)
                    {
                        border[i, j] = 1;
                        continue;
                    }
                    if (j < 4 && visitedMap[i, j + 1] == 1)
                    {
                        border[i, j] = 1;
                        continue;
                    }
                }
            }
            return border;
        }
        static void Main(string[] args)
        {
            int[,] visitedMap = { { 1, 1, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 } };
            int[,] border = GetBorderTargets(visitedMap);
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write(border[i, j]);
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
        }
    }
}
